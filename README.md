# recipes



## Recipes
My own food recipes in a git repository.

## Description
List of ingredients and procedures to obtain delicious home made food.
Filenames should give extra information, for example a recipe for a large loaf of bread that uses yeast is called bread-loaf-yeast.md
A similar recipe that uses sourdough should be called bread-load-sourdough.md and so on.

## Pictures
Picture or didn't happen applies to food.

## Roadmap
Initially there will be bread and pizza recipes, but adding a wider variety of recipes is a goal.

## Contributing
If you want to share your own versions or improve my recipes, let me know :)

## Author(s)
At the time of the first commit it's a solo project.

## License
Public Domain.

## Project status
This idea has been in my TODO list for years, today I decided to begin by creating this repository and publishing my first bread recipe.
