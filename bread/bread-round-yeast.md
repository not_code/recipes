# 8 medium size breads

## Ingredients and quantities


- Yeast (fresh)            1 unit
- Wheath flour (T45)       600 g
- Water+secret ingredient* 400 ml
- Salt                     1 pinch
- Sugar                    1 pinch

* Secret ingredient is one or more of the following:
  - Yogurt
  - Milk
  - Fromage blanc
  - Almond milk
  - Butter
  - Olive oil
  - Cream
  Anything that makes the dough fatter and stickier.
  300ml of water + 100ml of secret ingredient works for almost anything, but the best is to test different ratios until it's perfect.


## Materials and tools

- An oven
- A large bowl
- An oven cooking tray
- A tablespoon
- A wooden spoon


## Procedure

- Mix the water+secret ingredient, the sugar and the yeast thoughly by slowly stiring with a wooden spoon for approximatey 3 minutes.
- Add the flour. I prefer to add it with a tablespoon and mixing slowly and thoroughly untils the mix' viscosity does not allow me to use the spoon.
- Pre-heat the oven to 220°C.
- Kneat the dough and form a long cylinder of 4cm in diameter.
- Cut the cylinder in half.
- Repeat until you have 8 pieces of dough.
- Form 8 spheres.
- Put the dough spheres the cooking tray, add some flour if necessary.
- Bake for 30 minutes or until the crust is light brown.
- Eat.
