# Large loaf of bread

## Ingredients and quantities


- Yeast (fresh)         1 unit
- Wheath flour (T45)    600 g
- Water                 350 ml
- Salt                  1 pinch
- Sugar                 1 pinch

## Materials and tools

- An oven
- A large bowl
- A bread cooking pan
- A tablespoon
- A wooden spoon
- A flat surface (50x50cm should be enough).


## Procedure

- Mix the water, the sugar and the yeast thoughly by slowly stiring with a wooden spoon for approximatey 3 minutes.
  HINT: You can do it directly in the cooking pan.
- Leave the mixture for a few minutes, depending on ambient temperature, until you see bubbles or clusters of yeast.
- Add the flour. I prefer to add it with a tablespoon and mixing slowly and thoroughly untils the mix' viscosity does not allow me to use the spoon.
- Leave the dough for a few hours. Cover the cooking pan. In winter I leave it over night.
- Pre-heat the oven to 240°C.
- Knead the dough for a few minutes and make a regular sphere.
- Bake for 42 minutes or so, you know your oven better than me.

Then, once the bread is baked, wait for it to cool down and it should be safe to eat.

## Picture
https://masto.bike/system/media_attachments/files/109/666/944/348/598/814/original/5ce9db061916031e.jpg
