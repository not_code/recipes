# pizza dough

## Ingredients and quantities

- Yeast (fresh)         1 unit
- Wheat flour (T45)     300 g
- Water                 150 ml
- Salt                  1 pinch
- Olive oil             3 tablespoons


## Materials and tools

- An oven
- A baking pizza stone
- A tablespoon
- A flat surface (50x50cm should be enough)

## Procedure

- Mix the water, the yeast, the olive oil and the flour.
- Pre-heat the oven to 245°C if you're making a pizza.
- Knead until the dough is homogeneous, a few minutes are often enough.
- Make a sphere with the dough.
- Leave it a few minutes while you spread some flour in the pizza stone.
- With your bare hands make a flat circle with the dough.
    - add more flour on both sides until it's no longer sticky.
- Add you favorite pizza stuff
- Oven for 12 minutes (it really depends on your oven and stone) or until the dough is well baked and brown in the outside.
